#ifndef LIST_H
#define LIST_H

#include <stdint.h>
#include <stdbool.h>
#include "mem_alloc.h"

/*
 * References:
 * https://www.cs.princeton.edu/courses/archive/spr99/cs217/4/index.html
 * https://en.cppreference.com/w/cpp/container/list
*/

/**
 * @brief List ADT.
 * A (reference to a) list object.
 */
typedef struct ListStruct* List_t;

/**
 * @brief
 * A ListIterator ADT.
 * An iterator (accessor-mutator) to a list object.
 */
typedef struct ListIteratorStruct* ListIterator_t;

/**
 * @brief
 * Returns a created list.
 * NOTE: The creation process uses the allocator to set up some reserve space for list meta-data (private).
 */
List_t list_construct(mem_alloc_allocate allocator, mem_alloc_deallocate deallocator, size_t elementSize);

/**
 * @brief
 * Destroys the list freeing all of it's memory including list meta-data (private) memory and all elements' memory.
 */
void list_destroy(List_t list);

/**
 * @brief
 * Returns number of elements is the list.
 */
uint32_t list_count(List_t list);

/**
 * @brief
 * Returns number of bytes used up by list including list meta-data (private) size and aggregated elements size.
 */
size_t list_size(List_t list);

/**
 * @brief
 * Returns an iterator pointing to the first element in the List_t argument.
 * Returns the past-the-end (aka Post End Of List) marker in the list container if the list is empty.
 * Results in undefined behaviour if the argument List_t is invalid.
 */
ListIterator_t list_begin(List_t list);

/**
 * @brief
 * Returns the past-the-end (aka Post End Of List) marker in the list container.
 * Useful for checking if the iterator has gone beyond the end of the list.
 * Useful for inserting an element as the last node, i.e. at the end of the list.
 * Results in undefined behaviour if the argument List_t is invalid.
 */
ListIterator_t list_end(List_t list);

/**
 * @brief 
 * Returns an iterator to the next element to the argument.
 * Returns the past-the-end (aka Post End Of List) marker the argument is refering to last elemenent of list. 
 * Returns the past-the-end (aka Post End Of List) marker the argument is the past-the-end (aka Post End Of List) marker. 
 * Results in undefined behaviour if the argument ListIterator_t is invalid.
 */
ListIterator_t list_next(ListIterator_t listIterator);

/**
 * @brief
 * Inserts (creates and allocates) an element in the List_t argument at where ListIterator_t argument is referencing.
 * If the ListIterator_t argument is refering to yhe past-the-end (aka Post End Of List) marker then the element will be inserted as last element.
 * Results in undefined behaviour if the arguments List_t or ListIterator_t are invalid.
 */
void list_insert(List_t list, ListIterator_t listIterator, void* data);

/**
 * @brief
 * Erases (removes and dellaocates) element in the List_t argument at where ListIterator_t argument is referencing.
 * If the ListIterator_t argument is refering to past-the-end (aka Post End Of List) marker, it will lead to undefined behaviour.
 * Results in undefined behaviour if the arguments List_t or ListIterator_t are invalid.
 */
void list_erase(List_t list, ListIterator_t listIterator);

/**
 * @brief
 * Returns pointer to allocated data in element to which the argument ListIterator_t is referencing.
 * Returns NULL if the ListIterator_t argument is refering to the past-the-end (aka Post End Of List) marker.
 * Results in undefined behaviour if the argument ListIterator_t is invalid.
 */
void* list_element(ListIterator_t listIterator);

#endif //LIST_H
