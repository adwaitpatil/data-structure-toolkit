#ifndef MEM_ALLOC
#define MEM_ALLOC

#include <stdint.h>

typedef void* (*mem_alloc_allocate)(size_t size);
typedef void (*mem_alloc_deallocate)(void *ptr);

#endif //MEM_ALLOC