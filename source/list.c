#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "mem_alloc.h"
#include "list.h"

struct ListIteratorStruct
{
    void* data;
    ListIterator_t next;
};

struct ListIteratorStruct gPostEndOfList = {.data=NULL, .next=NULL};           //Variable for Post End Of List (marker).

#define PEOL     (&gPostEndOfList)  //Post End Of List (marker).

struct ListStruct
{
    uint32_t elementCount;
    size_t elementSize;
    size_t totalSize;
    ListIterator_t HEAD;
    ListIterator_t END;
    mem_alloc_allocate allocator;
    mem_alloc_deallocate deallocator;
};

List_t list_construct(mem_alloc_allocate allocator, mem_alloc_deallocate deallocator, size_t elementSize)
{
    List_t list = (List_t) allocator(sizeof(struct ListStruct));
    list->HEAD = PEOL;
    list->END = PEOL;
    list->allocator = allocator;
    list->deallocator = deallocator;
    list->elementCount = 0;
    list->elementSize = elementSize;
    list->totalSize = sizeof(struct ListStruct);
    return list;
}

void list_destroy(List_t list)
{
    assert ( NULL != list );

    if ( PEOL == list->HEAD )
    {
        list->deallocator(list);
        return;
    }

    ListIterator_t it = list->HEAD;
    ListIterator_t itNext = it->next;
    while ( itNext != PEOL )
    {
        list->deallocator(it->data);
        list->deallocator(it);
        it = itNext;
        itNext = itNext->next;
    }
    list->deallocator(it->data);
    list->deallocator(it);
    list->deallocator(list);
}

uint32_t list_count(List_t list)
{
    assert ( NULL != list );
    return list->elementCount;
}

size_t list_size(List_t list)
{
    assert ( NULL != list );
    return list->totalSize;
}

ListIterator_t list_begin(List_t list)
{
    assert ( NULL != list );
    return list->HEAD;
}

ListIterator_t list_end(List_t list)
{
    assert ( NULL != list );
    return PEOL;
}

ListIterator_t list_next(ListIterator_t listIterator)
{
    assert ( NULL != listIterator );
    if ( PEOL == listIterator )
        return PEOL;
    listIterator = listIterator->next;
    return listIterator;
}

void list_insert(List_t list, ListIterator_t listIterator, void* data)
{
    assert ( NULL != list && NULL != listIterator );

    ListIterator_t tmpListIterator = listIterator;
    ListIterator_t listNext = listIterator;
    ListIterator_t listPrev = list->HEAD;
    while ( PEOL != listPrev )
    {
        if ( listPrev->next == listIterator )
            break;
        listPrev = listPrev->next;
    }
    listIterator = (ListIterator_t) list->allocator(sizeof(struct ListIteratorStruct));
    listIterator->data =  list->allocator(list->elementSize);
    memcpy(listIterator->data, data, list->elementSize);

    if ( PEOL == list->HEAD )
    {
        listIterator->next = PEOL;
        list->HEAD = listIterator;
        list->END = listIterator;
    }
    else if ( PEOL == tmpListIterator )
    {
        listIterator->next = PEOL;
        list->END->next = listIterator;
        list->END = listIterator;
    }
    else if ( list->HEAD == tmpListIterator )
    {
        listIterator->next = list->HEAD;
        list->HEAD = listIterator;
    }
    else
    {
        listIterator->next = listNext;
        listPrev->next = listIterator;
    }
    list->elementCount += 1;
    list->totalSize += sizeof(struct ListIteratorStruct) + list->elementSize;
}

void list_erase(List_t list, ListIterator_t listIterator)
{
    assert ( NULL != list && NULL != listIterator );
    
    if( PEOL == list->HEAD || PEOL == listIterator )
    {
        return;
    }
    else if ( PEOL == list->HEAD->next )
    {
        list->deallocator(list->HEAD->data);
        list->deallocator(list->HEAD);
        list->HEAD = PEOL;
        list->END = PEOL;
    }
    else
    {
        ListIterator_t itNext = listIterator->next;
        ListIterator_t itPrev = list->HEAD;
        while ( itPrev->next != listIterator )
            itPrev = itPrev->next;
        list->deallocator(listIterator->data);
        list->deallocator(listIterator);
        if ( list->HEAD == listIterator )
            list->HEAD = itNext;
        else
            itPrev->next = itNext;
    }
    list->elementCount -= 1;
    list->totalSize -= sizeof(struct ListIteratorStruct) + list->elementSize;
}

void* list_element(ListIterator_t listIterator)
{
    assert ( listIterator != NULL );
    if( PEOL == listIterator )
        return NULL;
    return listIterator->data;
}
