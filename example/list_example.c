#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include "../source/list.h"

#define ELEMENT_SIZE_MAX    (100)

void printList(List_t list)
{
    ListIterator_t it;
    printf("List [cnt=%"PRIu32" size=%zd] -> ", list_count(list), list_size(list));
    for ( it=list_begin(list) ; it!=list_end(list) ; it=list_next(it) )
    {
        char* s = (char*)(list_element(it));
        printf("%s ", s);
    }
    printf("\n\r");
}

int main(int argc, char** argv)
{
    printf("\n\r\n\rList example.\n\n\r");
    ListIterator_t it;
    List_t list;
    
    printf("I. Create an empty list and then destroy it.\n\r");
    list = list_construct(malloc, free, ELEMENT_SIZE_MAX);
    printList(list);
    list_destroy(list);
    printf("List destroyed.\n\r");
    printf("\n\r");

    printf("II. Create a single element list and then destroy it.\n\r");
    list = list_construct(malloc, free, ELEMENT_SIZE_MAX);
    printList(list);
    it = list_begin(list);
    list_insert(list, it, "Zain");
    printList(list);
    list_destroy(list);
    printf("List destroyed.\n\r");
    printf("\n\r");

    printf("III. Create an empty list, then insert at end, then erase at begining.\n\r");
    list = list_construct(malloc, free, ELEMENT_SIZE_MAX);
    printList(list);
    it = list_end(list);
    list_insert(list, it, "Yasir");
    printList(list);
    it = list_begin(list);
    list_erase(list, it);
    printList(list);
    printf("\n\r");

    printf("IV. Insert few members (Bob to begining, then Alice to begining, then Charlie to end)\n\r");
    it = list_begin(list);
    list_insert(list, it, "Bob");
    printList(list);
    it = list_begin(list);
    list_insert(list, it, "Alice");
    printList(list);
    it = list_end(list);
    list_insert(list, it, "Charlie");
    printList(list);
    printf("\n\r");

    printf("V. Try to find Daisy\n\r");
    for ( it = list_begin(list) ; it != list_end(list) ; it = list_next(it) )
    {
        if ( 0 == strcmp("Daisy", (char*)(list_element(it))) )
            break;
    }
    if( it == list_end(list) )
        printf("Daisy was not found!\n\r");
    else
        printf("Daisy was found!\n\r");
    printList(list);
    printf("\n\r");

    printf("VI. Find and erase Bob\n\r");
    for ( it = list_begin(list) ; it != list_end(list) ; it = list_next(it) )
    {
        if ( 0 == strcmp("Bob", (char*)(list_element(it))) )
            break;
    }
    if( it == list_end(list) )
        printf("Bob was not found!\n\r");
    else
    {
        list_erase(list, it);
        printf("Bob was found and erased.\n\r");
    }
    printList(list);
    printf("\n\r");

    printf("VI. Erase Charlie and then Alice.\n\r");    
    for ( it = list_begin(list) ; it != list_end(list) ; it = list_next(it) )
    {
        if ( 0 == strcmp("Charlie", (char*)(list_element(it))) )
        {
            printf("Charlie found and erased.\n\r");
            list_erase(list, it);
            printList(list);
            break;
        }
    }
    it = list_begin(list);
    char str[ELEMENT_SIZE_MAX];
    memcpy(str, list_element(it), ELEMENT_SIZE_MAX);
    list_erase(list, it);
    printf("%s was remaining and is now erased.\n\r", str);
    printList(list);
    printf("\n\r");

    return 0;
}
